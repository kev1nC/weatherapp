import 'package:flutter/material.dart';

const kLocationNameStyle = TextStyle(
  fontSize: 40.0,
  fontWeight: FontWeight.bold,
);

const kTempStyle = TextStyle(
  fontSize: 30.0,
  fontWeight: FontWeight.bold,
);

const kHumidityStyle = TextStyle(
  fontSize: 30.0,
  fontWeight: FontWeight.bold,
);
