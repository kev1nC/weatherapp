import 'package:flutter/material.dart';
import 'package:weatherapp/screens/loading_screen.dart';
import 'login_screen.dart';
import 'package:weatherapp/services/signin.dart';
import 'package:weatherapp/services/constants.dart';
import 'package:weatherapp/widgets/card.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({this.latitude, this.longitude, this.locationWeather});

  final locationWeather;
  final double latitude;
  final double longitude;
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String locationName;
  String weatherIcon;
  String temperature;
  String humidity;
  String wind;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    updateInfo(widget.locationWeather);
  }

  void updateInfo(dynamic weatherData) {
    // do soemthing here
    locationName = weatherData['name'];
    weatherIcon = weatherData['weather'][0]['icon'];
    temperature = weatherData['main']['temp'].toString();
    humidity = weatherData['main']['humidity'].toString();
    wind = weatherData['wind']['speed'].toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.refresh),
          onPressed: () {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) {
              return LoadingScreen();
            }));
          },
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: () {
              signOutGoogle();
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) {
                return LoginScreen();
              }), ModalRoute.withName('/'));
            },
          ),
        ],
      ),
      extendBodyBehindAppBar: true,
      body: SafeArea(
        child: Align(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '$locationName',
                style: kLocationNameStyle,
              ),
              SizedBox(height: 10.0),
              Image.network(
                'http://openweathermap.org/img/wn/${weatherIcon}@2x.png',
              ),
              Text(
                "🌡  " + temperature + " °C",
                style: kTempStyle,
              ),
              SizedBox(height: 20.0),
              InfoCard(
                  property: '💦 Humidity: ', propertyValue: '${humidity} %'),
              SizedBox(height: 10.0),
              InfoCard(property: '💨 Wind: ', propertyValue: '${wind} km/h'),
            ],
          ),
        ),
      ),
    );
  }
}

// onPressed: () {
//                   signOutGoogle();
//                   Navigator.of(context).pushAndRemoveUntil(
//                       MaterialPageRoute(builder: (context) {
//                     return LoginScreen();
//                   }), ModalRoute.withName('/'));
//                 },
