import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:weatherapp/services/location.dart';
import 'package:weatherapp/screens/homescreen.dart';
import 'package:weatherapp/services/networking.dart';

const APIkey = "48bb255dcf42275998ecfa024e598b3c";

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLocation();
  }

  void getLocation() async {
    UserLocation loc = UserLocation();
    await loc.getCurrentLocation();

    final lat = loc.latitude;
    final long = loc.longitude;

    NetworkHelper networkHelper = NetworkHelper(
        "https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&appid=$APIkey&units=metric");

    var weatherData = await networkHelper.getData();

    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
      return HomeScreen(
        latitude: loc.latitude,
        longitude: loc.longitude,
        locationWeather: weatherData,
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SpinKitRipple(
          color: Colors.deepOrange,
          size: 250.0,
        ),
      ),
    );
  }
}
