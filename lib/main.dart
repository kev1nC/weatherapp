import 'package:flutter/material.dart';
import 'package:weatherapp/screens/loading_screen.dart';
import 'screens/login_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: LoginScreen(),
      routes: {
        '/loadingscreen': (_) => new LoadingScreen(),
      },
    );
  }
}
